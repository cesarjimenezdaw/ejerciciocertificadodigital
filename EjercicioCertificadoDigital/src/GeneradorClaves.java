import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;

public class GeneradorClaves {
    /*
    Crea una primera aplicación muy sencilla, en la clase GeneradorClaves que cree un par de claves
    pública y privada para cifrar con el cifrado asimétrico. Guárdalas cada una en un fichero distinto:
    publicKey y privateKey. Utiliza el algoritmo RSA.
    * */

    public static void generadorDeClaves(){
        String algoritmo = "RSA";

        try {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance(algoritmo);

            KeyPair keyPair = keygen.genKeyPair();

            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();

            FileOutputStream publicKeyStream = new FileOutputStream("publicKey");
            FileOutputStream privateKeyStream = new FileOutputStream("privateKey");

            byte[] publicKeyBytes = publicKey.getEncoded();
            byte[] privateKeyBytes = privateKey.getEncoded();

            publicKeyStream.write(publicKeyBytes);
            privateKeyStream.write(privateKeyBytes);

            publicKeyStream.close();
            privateKeyStream.close();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
