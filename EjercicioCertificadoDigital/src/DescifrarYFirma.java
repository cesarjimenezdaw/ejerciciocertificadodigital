import javax.crypto.*;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;

public class DescifrarYFirma {
    public static void descifrarYFirmar(String nombrePublicKey) {
        try {
            PublicKey publicKey = leerClavePublica(nombrePublicKey);

            // Leer mensaje cifrado y firma
            byte[] mensajeCifrado = leerMensaje("mensajeCifrado");
            byte[] firmaMensaje = leerMensaje("firmaMensaje");

            ArrayList<byte[]> mensajes = new ArrayList<>();
            ArrayList<String> nombresArchivos = new ArrayList<>();

            mensajes.add(mensajeCifrado);
            nombresArchivos.add("mensajeSinFirma");

            mensajes.add(firmaMensaje);
            nombresArchivos.add("Mensaje CON FIRMA");

            for (int i = 0; i < mensajes.size(); i++) {
                boolean firmaValida = verificarFirma(mensajes.get(i), firmaMensaje, publicKey);
                System.out.println("Comprobando - " + nombresArchivos.get(i));

                if (!firmaValida) {
                    System.out.println("firma incorrecta");
                }else{
                    // Descifrar mensaje cifrado con clave publica
                    byte[] mensajeDescifrado = descifrarMensaje(mensajes.get(i), publicKey);
                    System.out.println("Mensaje descifrado: " + new String(mensajeDescifrado));
                }
            }

        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException |
                 NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException |
                 BadPaddingException | SignatureException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static PublicKey leerClavePublica(String filename) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        FileInputStream fis = new FileInputStream(filename);
        byte[] claveBytes = new byte[fis.available()];
        fis.read(claveBytes);
        fis.close();

        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(new X509EncodedKeySpec(claveBytes));
    }

    public static byte[] leerMensaje(String nombreArchivo) throws IOException {
        FileInputStream fis = new FileInputStream(nombreArchivo);
        byte[] datos = new byte[fis.available()];
        fis.read(datos);
        fis.close();
        return datos;
    }

    public static boolean verificarFirma(byte[] mensaje, byte[] firma, PublicKey publicKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(publicKey);
        signature.update(mensaje);
        return signature.verify(firma);
    }

    public static byte[] descifrarMensaje(byte[] mensajeCifrado, PublicKey publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        return cipher.doFinal(mensajeCifrado);
    }
}
