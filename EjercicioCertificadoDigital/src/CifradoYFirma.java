import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Scanner;
import javax.crypto.*;
import java.util.Base64;

public class CifradoYFirma {
    public static void cifrarYFirmar() {
        Scanner sc = new Scanner(System.in);
        String mensaje = sc.nextLine();

        try {
            //guardamos la clave privada que esta en el archivo "elqusea" para usarla para encriptar
            PrivateKey privateKey = leerClavePrivada("privateKey");

            // Cifrar el mensaje con la clave privada
            byte[] mensajeCifrado = cifrarMensaje(mensaje, privateKey);

            // Firmar el mensaje con la clave privada y el algoritmo SHA256withRSA
            byte[] firmaMensaje = firmarMensaje(mensaje, privateKey);

            // Guardar el mensaje cifrado en un archivo llamado "mensajeCifrado"
            guardarEnArchivo("mensajeCifrado", mensajeCifrado);

            // Guardar la firma del mensaje en un archivo llamado "firmaMensaje"
            guardarEnArchivo("firmaMensaje", firmaMensaje);

            System.out.println("Mensaje cifrado y firma generada correctamente.");

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException |
                 NoSuchPaddingException | IllegalBlockSizeException |
                 BadPaddingException | SignatureException |
                 InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    // Método para leer la clave privada desde el archivo que se pase
    public static PrivateKey leerClavePrivada(String filename) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        FileInputStream fis = new FileInputStream(filename);
        byte[] claveBytes = new byte[fis.available()];
        fis.read(claveBytes);
        fis.close();

        KeyFactory kf = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(claveBytes);
        return kf.generatePrivate(keySpecPKCS8);
    }

    // Método para cifrar el mensaje con la clave privada
    public static byte[] cifrarMensaje(String mensaje, PrivateKey privateKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        return cipher.doFinal(mensaje.getBytes());
    }

    // Método para firmar el mensaje con la clave privada y el algoritmo SHA256withRSA
    public static byte[] firmarMensaje(String mensaje, PrivateKey privateKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature firma = Signature.getInstance("SHA256withRSA");
        firma.initSign(privateKey);
        firma.update(mensaje.getBytes());
        return firma.sign();
    }

    // Método para guardar un array de bytes en un archivo codificado en Base64
    public static void guardarEnArchivo(String nombreArchivo, byte[] data) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(nombreArchivo)) {
            //byte[] encodedData = Base64.getEncoder().encode(data);
            //fos.write(encodedData);
            fos.write(data);
        }
    }
}
